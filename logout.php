<?php
	require_once 'config/init.php';
	session_destroy();

	header("location: index.php");
?>