<?php 
session_start();

require_once 'config.php';

function __autoload($class_name) {
  require_once 'includes/classes/'.$class_name . '.php';
}

$connection = mysqli_connect ($db['hostname'], $db['username'], $db['password'], $db['database']) or exit ("Unable to connect to database!");