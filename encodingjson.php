<?php 
require 'config/init.php';
$connection = mysqli_connect ($db['hostname'], $db['username'], $db['password'], $db['database']) or exit ("Unable to connect to database!");

/*
$sql = "SELECT * FROM Footballers"; //replace with your table name 


$result = mysqli_query($connection,$sql);
$jsondata = array();
while($row=mysqli_fetch_row($result)){
$jsondata[]=$row;
}
echo json_encode($jsondata);

*/

$query =
  "SELECT 
  ID, 
  Name, 
  DOB, 
  Team,
  League 
  FROM Footballers ORDER BY ID ASC";

$rsPackages = mysqli_query($connection,$query);

$arRows = array();
while ($row_rsPackages = mysqli_fetch_assoc($rsPackages)) {
  array_push($arRows, $row_rsPackages);
}

//header('Content-type: application/json');
echo json_encode($arRows);


?>