<!-- Edited with XML Spy v4.2 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
	<xsl:template match="/">
	
	<html>
		
		<body>
			
			<h2>Footballers</h2>
			
			<table border="1">
				
				<tr bgcolor="green">

					<th align="left">ID</th>
					<th align="left">Name</th>
					<th align="left">DOB</th>
					<th align="left">Team</th>
					<th align="left">League</th>

				</tr>
				
			<xsl:for-each select="Footballers/Footballer">
				<tr>
					
					<td>
						<xsl:value-of select="ID"/>
					</td>
					
					<td>
						<xsl:value-of select="Name"/>
					</td>

					<td>
						<xsl:value-of select="DOB"/>
					</td>

					<td>
						<xsl:value-of select="Team"/>
					</td>

					<td>
						<xsl:value-of select="League"/>
					</td>

				</tr>
			</xsl:for-each>

			</table>

		</body>

	</html>

</xsl:template>
</xsl:stylesheet>