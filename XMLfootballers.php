<?php require './includes/header.php'; ?>

<?php require './includes/nav.php'; ?>



<?php 
header ("Content-Type:text/xml");//Tell browser to expect xml

//include ("db_connect.php");
$connection = mysqli_connect ($db['hostname'], $db['username'], $db['password'], $db['database']) or exit ("Unable to connect to database!");

$query = "SELECT * FROM Footballers"; 
$result = mysqli_query($connection, $query) or die ("Error in query: $query. ".mysql_error()); 

//Top of xml file
$_xml = '<?xml version="1.0"?>'; 
// the code below this line is not needed for this to work, but I am keeping it just incase i need it in the future.
$_xml .= '<?xml-stylesheet type="text/xsl" href="XSLfootballers.xsl"?>' ."\n";

$_xml .="<Footballers>"; 
		while($row = mysqli_fetch_array($result)) 
		{ 
			$_xml .="<Footballer>"; 
			$_xml .="<ID>".$row['ID']."</ID>"; 
			$_xml .="<Name>".$row['Name']."</Name>"; 
			$_xml .="<DOB>".$row['DOB']."</DOB>";
			$_xml .="<Team>".$row['Team']."</Team>";
			$_xml .="<League>".$row['League']."</League>";
			$_xml .="</Footballer>"; 
		} 
$_xml .="</Footballers>"; 

file_put_contents("./assets/xml/footballers.xml", $_xml)
//Parse and create an xml object using the string
//$xmlobj=new SimpleXMLElement($_xml);
//And output
//print $xmlobj->asXML();
//or we could write to a file
//$xmlobj->asXML(winerys.xml);
?>

<?php require './includes/footer.php'; ?>