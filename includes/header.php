<?php require_once 'config/init.php';?>

<!DOCTYPE html>

<html lang="en">

	<head>

		<!-- Basic Page Needs
	  ================================================== -->
		<meta charset="utf-8">
		<title> K44YAR </title>
		

		<!-- Mobile Specific Metas
	  ================================================== -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<!-- CSS
	  ================================================== -->
		<link rel="stylesheet" href="assets/css/base.css">
		<link rel="stylesheet" href="assets/css/skeleton.css">
		<link rel="stylesheet" href="assets/css/layout.css">


		<!-- Favicons
		================================================== -->
		<link rel="shortcut icon" href="assets/images/favicon.ico">
		<link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
		<link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png">

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="../AIDc3361434/assets/js/example.js"></script>

		<!--
		<link rel="stylesheet" href="colorbox.css">
        <script src="jquery.min.js"></script>
        <script src="jquery.colorbox-min.js"></script>
		-->

		<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCturn3TxUuC7H24w3PSZlIk5Hh1UHiUKE&amp;sensor=FALSE&amp;libraries=places"></script>

		<!-- bottom code added
		<script src="../assets/js/google.js"></script> -->

		<script>
		function initialize()
		{

			var mapOptions = {
								  center:new google.maps.LatLng(53.803642,-1.547429),
								  zoom:12,
								  mapTypeId:google.maps.MapTypeId.ROADMAP
			  				 };
			
			var map = new google.maps.Map(document.getElementById("map"),mapOptions);

			var markerOptions = {
    								position: new google.maps.LatLng(53.803642,-1.547429)
								};
			
			var marker = new google.maps.Marker(markerOptions);
			marker.setMap(map);

			var infoWindowOptions = {
		    							content: 'London!'
									};

			var infoWindow = new google.maps.InfoWindow(infoWindowOptions);
			
			google.maps.event.addListener(marker,'click',function(e)
			{
		  
				infoWindow.open(map, marker);
		  
			});

			var acOptions = {
							  types: ['establishment']
							};
			var autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'),acOptions);
			
			autocomplete.bindTo('bounds',map);
			
			var infoWindow = new google.maps.InfoWindow();
			
			var marker = new google.maps.Marker({
												  	map: map
												});

			google.maps.event.addListener(autocomplete, 'place_changed', function() 
			{
			  infoWindow.close();
			  
			  var place = autocomplete.getPlace();
			  
			  if (place.geometry.viewport) 
			  {
			    map.fitBounds(place.geometry.viewport);
			  } 

			  else 
			  {
			    map.setCenter(place.geometry.location);
			    map.setZoom(17);
			  }
			  
			  marker.setPosition(place.geometry.location);
			  
			  infoWindow.setContent('<div><strong>' + place.name + '</strong><br>');
			  
			  infoWindow.open(map, marker);
			  
			  google.maps.event.addListener(marker,'click',function(e)
			  {
			   	 infoWindow.open(map, marker);
			  });

			});

			var weatherLayer = new google.maps.weather.WeatherLayer({
 													 temperatureUnits: google.maps.weather.TemperatureUnit.CELSIUS
																	});
			weatherLayer.setMap(map);

			var cloudLayer = new google.maps.weather.CloudLayer();
			
			cloudLayer.setMap(map);

		}

		google.maps.event.addDomListener(window, 'resize', initialize);
		google.maps.event.addDomListener(window, 'load', initialize);
		</script>

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>

		<!-- following is for the slideshow jquery pluggin -->
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		
		<!-- ../assets was added to the below src-->
		<script src="../assets/js/modernizr.custom.63321.js"></script>

		<script type="text/javascript" src="form.js"></script>

		
		<!-- Ajax -->
		<script type="text/javascript" src="assets/js/jquery-1.9.0.min.js"></script>
		
		<script type="text/javascript">
		$(document).ready(function() {
			$("#username").keyup(function (e) {
			
				//removes spaces from username
				$(this).val($(this).val().replace(/\s/g, ''));
				
				var username = $(this).val();
				if(username.length < 4){$("#user-result").html('');return;}
				
				if(username.length >= 4){
					$("#user-result").html('<img src="assets/images/imgs/ajax-loader.gif" />');
					$.post('check_username.php', {'username':username}, function(data) {
					  $("#user-result").html(data);
					});
				}
			});	
		});
		</script>

		
		<!-- Ajax -->
		<script type="text/javascript" src="assets/js/jquery-1.9.0.min.js"></script>
		
		<script type="text/javascript">
		$(document).ready(function() {
			$("#email").keyup(function (e) {
			
				//removes spaces from username
				$(this).val($(this).val().replace(/\s/g, ''));
				
				var email = $(this).val();
				if(email.length < 4){$("#email-result").html('');return;}
				
				if(email.length >= 4){
					$("#email-result").html('<img src="assets/images/imgs/ajax-loader.gif" />');
					$.post('check_email.php', {'email':email}, function(data) {
					  $("#email-result").html(data);
					});
				}
			});	
		});
		</script>



		  <style>
		    * { margin:0; padding:0; } /* a simple reset */
		    .head, li, h2 { margin-bottom:15px; }
		    .head { display:block; }
		    .content { display:none; }
		  </style>
		  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		  <script>
		    $(document).ready(function(){
		      $('.head').click(function(e){
		        e.preventDefault();
		        $(this).closest('li').find('.content').slideToggle();
		      });
		    });
		  </script>

	</head>


	<div class="container"> <!-- beginning of container, it ends on footer.php-->
		
		<div class ="headerr">
		<center>
		<header>
			
		           <div class="sixteen columns">
						<br/><h1 class="remove-bottom" style="margin-top: 40px">This is the header via include header.php</h1>
						<h5>Name: Abu Sufian Kayar <br/> ID: c3361434 </h5>

							<?php require 'usernav.php'; ?>

						<hr />
					</div>
			
		 </header>
		</center>
		</div>

<body>