<?php require './includes/header.php'; ?>

<?php require './includes/nav.php'; ?>

	<div class="two-thirds column">

		<h2> Item Class Test </h2>
		
		<?php
			print "<h4> List of Footballs Available <br /> <br/> </h4>";

			$ball1 = new Ball("Nike", "5", "grass", "professional");
			print "Brand: " . $ball1->getBrand();
			print "<br/ >";
			print "Size: " . $ball1->getSize();
			print "<br/ >";
			print "Surface: " . $ball1->getSurface();
			print "<br/ >";
			print "Level: " . $ball1->getLevel();
			print "<br /> <br />";




			$ball2 = new Ball("Addidas", "5", "artificial grass", "intermediate");
			print "Brand: " . $ball2->getBrand();
			print "<br/ >";
			print "Size: " . $ball2->getSize();
			print "<br/ >";
			print "Surface: " . $ball2->getSurface();
			print "<br/ >";
			print "Level: " . $ball2->getLevel();
			print "<br /> <br />";




			$ball3 = new Ball("Puma", "4", "artificial grass", "intermediate");
			print "Brand: " . $ball3->getBrand();
			print "<br/ >";
			print "Size: " . $ball3->getSize();
			print "<br/ >";
			print "Surface: " . $ball3->getSurface();
			print "<br/ >";
			print "Level: " . $ball3->getLevel();
			print "<br /> <br />";




			$ball4 = new Ball("Umbro", "3", "all surfaces", "amateur");
			print "Brand: " . $ball4->getBrand();
			print "<br/ >";
			print "Size: " . $ball4->getSize();
			print "<br/ >";
			print "Surface: " . $ball4->getSurface();
			print "<br/ >";
			print "Level: " . $ball4->getLevel();
			print "<br /> <br />";




			$ball5 = new Ball("Mitre", "4", "all surfaces", "amatuer");
			print "Brand: " . $ball5->getBrand();
			print "<br/ >";
			print "Size: " . $ball5->getSize();
			print "<br/ >";
			print "Surface: " . $ball5->getSurface();
			print "<br/ >";
			print "Level: " . $ball5->getLevel();
			print "<br /> <br /> ";
		?>

		

		<?php
			echo "<center> <table style='border: solid 1px black;'>";

			echo "<tr> 
					 <center>
						<th> Footballer ID </th>
						<th> Name </th>
						<th> DOB </th>
						<th> Team </th>
						<th> League </th>
					 </center>
				 </tr>";


			class TableRows extends RecursiveIteratorIterator 
			{ 
			    function __construct($it) 
			    { 
			        parent::__construct($it, self::LEAVES_ONLY); 
			    }

			    function current() 
			    {
			        return "<td style='width:150px;border:1px solid black;'> <center>" . parent::current(). "</center> </td>";
			    }

			    function beginChildren()
			    { 
			        echo "<tr>"; 
			    } 

			    function endChildren() 
			    { 
			        echo "</tr>" . "\n";
			    } 
			} 



			try 
			{
				    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
				    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				    $stmt = $conn->prepare("SELECT ID, Name, DOB, Team, League FROM Footballers"); 
				    $stmt->execute();

				    // set the resulting array to associative
				    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC); 

				    // this is the table for the search footballers
				    print '<br /> <center> <h4> PDO OO Prepared Statement MySqli and Basic Selection <br /> </h4> </center>
				    		<center> <h4> Footballers <br /> </h4> </center>';
					
				    foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) 
				    { 
				        echo $v;
			    	}

			    	$dsn = null;
			}

			catch(PDOException $e)    
			{
			    	echo "Error: " . $e->getMessage();
			}

			$conn = null;
			echo "</center> </table> <br />";

		?>






		<?php
			echo "<center> <table style='border: solid 1px black;'>";

			echo "<tr> 
					 <center>
						<th> Footballer ID </th>
						<th> Name </th>
						<th> DOB </th>
						<th> Team </th>
						<th> League </th>
					 </center>
				 </tr>";


			class TableRowsSelected extends RecursiveIteratorIterator 
			{ 
			    function __construct($it) 
			    { 
			        parent::__construct($it, self::LEAVES_ONLY); 
			    }

			    function current() 
			    {
			        return "<td style='width:150px;border:1px solid black;'> <center>" . parent::current(). "</center> </td>";
			    }

			    function beginChildren()
			    { 
			        echo "<tr>"; 
			    } 

			    function endChildren() 
			    { 
			        echo "</tr>" . "\n";
			    } 
			} 



			try 
			{
				    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
				    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				    $stmt = $conn->prepare("SELECT ID, Name, DOB, Team, League FROM Footballers WHERE Team = 'Manchester United'"); 
				    $stmt->execute();

				    // set the resulting array to associative
				    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC); 

				    // this is the table for the search footballers
				    print '<br /> <center> <h4> PDO OO Prepared Statement MySqli specific query <br /> </h4> </center>
				    		<center> <h4> Manchester United Footballers <br/> </h4> </center>';

				    foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) 
				    { 
				        echo $v;
			    	}

			    	$dsn = null;
			}

			catch(PDOException $e)    
			{
			    	echo "Error: " . $e->getMessage();
			}

			$conn = null;
			echo "</center> </table>";

		?>

</div>


	


<?php require './includes/footer.php'; ?>	
