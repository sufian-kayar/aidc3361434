<!DOCTYPE html>
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 

		<link rel="shortcut icon" href="../favicon.ico"> 
		<link rel="stylesheet" type="text/css" href="../AIDc3361434/assets/css/style.css" />
		<script src="js/modernizr.custom.63321.js"></script>
		<!--[if lte IE 7]><style>.support-note .note-ie{display:block;}</style><![endif]-->

		<link rel="stylesheet" href="../AIDc3361434/assets/css/base.css">
		<link rel="stylesheet" href="../AIDc3361434/assets/css/skeleton.css">
		<link rel="stylesheet" href="../AIDc3361434/assets/css/layout.css">

	</head>
	<body>
		<div class="container">	
			
			<section class="main">

				<ul id="st-stack" class="st-stack-raw">

					<li><div class="st-item"><img src="../AIDc3361434/assets/images/ronaldo.jpg" width="300" height="300"/></a></div><div class="st-title"><h2> Cristiano Ronaldo </h2></div></li>

					<li><div class="st-item"><img src="../AIDc3361434/assets/images/messi.jpg" width="310" height="300"/></a></div><div class="st-title"><h2> Lionel Messi </h2></div></li>

					<li><div class="st-item"><img src="../AIDc3361434/assets/images/zlatan.jpg" width="330" height="300"/></a></div><div class="st-title"><h2> Zlatan Ibrahimovic </h2></div></li>

					<li><div class="st-item"><img src="../AIDc3361434/assets/images/rooney.jpg" width="330" height="300"/></a></div><div class="st-title"><h2> Wayne Rooney </h2></div></li>

					<li><div class="st-item"><img src="../AIDc3361434/assets/images/falcao.jpg" width="300" height="350"/></a></div><div class="st-title"><h2> Radamel Falcaa </h2></div></li>

				</ul>
				<p>Tip: to see a continuous flow, keep your mouse pressed on the navigation arrows.</p>
			</section>

		</div><!-- /container -->
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script type="text/javascript" src="../AIDc3361434/assets/js/jquery.stackslider.js"></script>
		<script type="text/javascript">
			
			$( function() {
				
				$( '#st-stack' ).stackslider();

			});

		</script>
	</body>
</html>