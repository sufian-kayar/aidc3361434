<?php require './includes/header.php'; ?>

<?php require './includes/nav.php'; ?>

<?php
		$json_string = file_get_contents("http://api.wunderground.com/api/1eeed606cd644586/conditions/q/UK/Leeds.json");
		$parsed_json = json_decode($json_string, true);
		//print_r($parsed_json); //uncomment this to see all the data available
		
		echo "<br /> <br />";
		$location = $parsed_json['current_observation']['display_location']['city'];
		$state = $parsed_json['current_observation']['observation_location']['state'];
		$temp_c = $parsed_json['current_observation'] ['temp_c'];
		$image = $parsed_json['current_observation'] ['icon_url'];
		echo "<p> Current temp in " . $location . ", " . "(" . $state . ")" . " is: " . $temp_c . "</p>";
		echo "<img src=$image />";		
?>
						                <br /><br /> Example of Using Accordian JQuery<br /><br />
  					             		<ul>
                              <li>
                                <a href='#' class='head'>Heading 1</a>
                                <div class='content'>Content 1
                                </div>
                              </li>
                              <li>
                                <a href='#' class='head'>Heading 2</a>
                                <div class='content'>Content 2</div>
                              </li>
                              <li>
                                <a href='#' class='head'>Heading 3</a>
                                <div class='content'>Content 3</div>
                              </li>
                            </ul>


                          <br /> Example of Using DatePicker JQuery <br /><br />
                          <link href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/ui-darkness/jquery-ui.min.css" rel="stylesheet">
                          <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
                          <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>

                          <input id="datepicker" type="text">
                          <p id="dateoutput"></p>
                          <script>
                            /*
                             * jQuery UI Datepicker: Parse and Format Dates
                             * http://salman-w.blogspot.com/2013/01/jquery-ui-datepicker-examples.html
                             */
                            $(function() {
                              $("#datepicker").datepicker({
                                dateFormat: "dd-mm-yy",
                                onSelect: function(dateText, inst) {
                                  var date = $.datepicker.parseDate(inst.settings.dateFormat || $.datepicker._defaults.dateFormat, dateText, inst.settings);
                                  var dateText1 = $.datepicker.formatDate("D, d M yy", date, inst.settings);
                                  date.setDate(date.getDate() + 7);
                                  var dateText2 = $.datepicker.formatDate("D, d M yy", date, inst.settings);
                                  $("#dateoutput").html("Chosen date is <b>" + dateText1 + "</b>; chosen date + 7 days yields <b>" + dateText2 + "</b>");
                                }
                              });
                            });
                          </script>

<?php require './includes/footer.php'; ?>