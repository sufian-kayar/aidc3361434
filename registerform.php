<?php require './includes/header.php'; ?>

<?php require './includes/nav.php'; ?>

<div class="two-thirds column">		
		<form method="post" action="registerform.php">
		
			<h3>Register</h3>
			Email <input type="text" id="email" name="c_reg_email" value='' required /> <span id="email-result"></span><br />

			Username <input type="text" id="username" name="c_reg_username" value='' required/> <span id="user-result"></span><br />
			
			Password <input type="password" name="c_reg_password" required /> <br />
			
			Confirm Password <input type="password" name="c_reg_password2" required /> <br />
			
			<input type="submit" name="submit" value="submit" />
			
				<?php
					echo $_SESSION['errors'];
				?>
		
		</form>
</div>

<?php

				require_once './config/init.php';
				
				//print 'works';

				$connection = mysqli_connect($db['hostname'], $db['username'], $db['password'], $db['database']) or exit ("Unable to connect to database!");
				
				//initialise array to hold error messages
				//$error_check = array();
				
				if (isset($_POST['c_reg_email']) && isset($_POST['c_reg_username']) && isset($_POST['c_reg_password']) && isset($_POST['c_reg_password2']))
				{
					$c_reg_email = $_POST['c_reg_email'];
					$c_reg_name = $_POST['c_reg_username'];
					$c_reg_password = $_POST['c_reg_password'];
					$c_reg_password2 = $_POST['c_reg_password2'];
					
					$_SESSION['$c_reg_email'] = $c_reg_email;
					$_SESSION['$c_reg_name'] = $c_reg_name;
					$_SESSION['$c_reg_password'] = $c_reg_password;
					$_SESSION['$c_reg_password2'] = $c_reg_password2;
					
					
					$c_reg_pwordhash = md5($c_reg_password2);
					
					//print_r($_POST);
					if (!empty($c_reg_email) && !empty($c_reg_name) && !empty($c_reg_password) && !empty($c_reg_password2))
					{
						if (strlen($c_reg_password2) < 3)
						{
							//$error .= 'Passwords is too short, must be longer than 3 letters <br /> <br />';
							echo 'Passwords is too short, must be longer than 3 letters <br /> <br />';
						}


						if ($c_reg_password != $c_reg_password2)
						{
								//$error .= 'Passwords do not match <br /> <br />';
								echo 'Passwords do not match <br /> <br />';
								//$password = false;
						}


						$errormail = 0;

						if (empty($_POST ['c_reg_email']) || !filter_var($c_reg_email, FILTER_VALIDATE_EMAIL))
						{
								//$error .= 'You must provide a valid email address <br /> <br />';
								echo 'You must provide a valid email address <br /> <br />';
								$errormail = 1;
						}

						$errorname = 0;

						if (empty($_POST ['c_reg_username']) || strlen($c_reg_name) < 3 || !preg_match("/^[a-zA-Z ]*$/", $c_reg_name))
						{
								//$error .= 'You must fill in your name, it mus be longer than 3 charcters. Only letters and whitespace allowed <br /> <br />';
								echo 'You must fill in your name, it must be longer than 3 charcters. Only letters and whitespace allowed <br /> <br />';
								$errorname = 1;
						}



							
						else 	
						{
							$query = "SELECT username FROM users WHERE username = '$c_reg_name'";
							$result =  mysqli_query($connection, $query);
							
							if (mysqli_num_rows($result) == 1)
							{
								//$error_check[] = '<center> <h4>Username already exists. </h4> </center>';
								echo '<center> <h4>Username already exists. </h4> </center>';
								//$username = false;
							}

							$query1 = "SELECT email FROM users WHERE email = '$c_reg_email'";
							$result1 =  mysqli_query($connection, $query1);
							
							if (mysqli_num_rows($result1) == 1)
							{
								//$error_check[] = '<center> <h4>Username already exists. </h4> </center>';
								echo '<center> <h4>Email already in use. </h4> </center>';
								//$username = false;
							}
							
							else 
							{
								$register = true;
								//$connection = mysqli_connect($db) or exit ("Unable to connect to database!");
								$query1 = "INSERT INTO users (username, password, access, email) VALUES ('$c_reg_name', '$c_reg_pwordhash', '0','$c_reg_email')";
								$result1 = mysqli_query($connection, $query1);
								echo '<center> <h4> You have Successfully Registered, now go Back to Home and Login </h4> </center>';
								mysqli_close ($connection);
							}
							
						}
							
					}
				
					else if (empty($_POST ['c_reg_email']) && empty($_POST ['c_reg_username']) && empty($_POST ['c_reg_password']) && empty($_POST ['c_reg_password2'])) 
					{
						//$register = false;
						//$error .= 'All fields required <br />';
						echo 'All fields required <br />';
					}
					
					//$_SESSION['errors'];
					
					//print ($error);

				}	
?>

<?php require './includes/footer.php'; ?>